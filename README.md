Simple blog project. Like CMS, written on PHP back-end and Angular on frontend. Was created as test projects on trainee position.

**Implemented Functionality:**

1. Post articles. CRUD operation: CREATE, DELETE, UPDATE posts and users.
2. Admin Panel.
3. Authorization: JWT Authorization, tokens, CryptoJS data.
4. Frontend Angular Components.
5. SEO optimization and pre-render Angular Universal.
6. Localization: EN, UA, RU.
7. Data Base: PhpMyAdmin, MySQL.
8. CSS layout: Angular flex-layout framework, mobile  responsive.

**Project Presentation:
<br>
Project Requirements:
<br>
Some foto:**


